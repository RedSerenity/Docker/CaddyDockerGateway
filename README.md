Caddy Docker Image with the following plugins already baked in:
---

Reverse Proxy Helper
* github.com/caddyserver/replace-response

Docker Gateway + SSL Certs
* github.com/pteich/caddy-tlsconsul
* github.com/lucaslorentz/caddy-docker-proxy/plugin/v2

Dns Providers
* github.com/caddy-dns/digitalocean
* github.com/caddy-dns/googleclouddns
* github.com/caddy-dns/openstack-designate
* github.com/caddy-dns/azure
* github.com/caddy-dns/gandi
* github.com/caddy-dns/route53
* github.com/caddy-dns/cloudflare

Authentication
* github.com/greenpau/caddy-auth-portal
* github.com/greenpau/caddy-authorize

Dynamic DNS
* github.com/mholt/caddy-dynamicdns


### Environment Variables

* ACME_CERT_EMAIL
* CONSUL_ADDR (localhost:8500)
* CONSUL_TIMEOUT (10)
* CONSUL_TOKEN
* CONSUL_PREFIX (caddytls)
* CONSUL_AES_KEY (consultls-1234567890-caddytls-32)
* CONSUL_TLS_ENABLED (false)
* CONSUL_INSECURE (true)
* ACME_PROVIDER (digitalocean)
* ACME_PROVIDER_OPTIONS
