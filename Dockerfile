ARG CADDY_VERSION="2.4.5"
FROM caddy:${CADDY_VERSION}-builder-alpine AS Builder

RUN xcaddy build \
  # Reverse Proxy Helper
  --with github.com/caddyserver/replace-response \
  # Docker Gateway + SSL Certs
  --with github.com/pteich/caddy-tlsconsul \
  --with github.com/lucaslorentz/caddy-docker-proxy/plugin/v2 \
  # Dns Providers
  --with github.com/caddy-dns/digitalocean \
  --with github.com/caddy-dns/googleclouddns \
  --with github.com/caddy-dns/openstack-designate \
  --with github.com/caddy-dns/azure \
  --with github.com/caddy-dns/gandi \
  --with github.com/caddy-dns/route53 \
  --with github.com/caddy-dns/cloudflare \
  # Authentication
  --with github.com/greenpau/caddy-auth-portal \
	--with github.com/greenpau/caddy-authorize \
  # Dynamic DNS
  --with github.com/mholt/caddy-dynamicdns

ARG CADDY_VERSION=${CADDY_VERSION}
FROM caddy:${CADDY_VERSION}-alpine

COPY --from=Builder /usr/bin/caddy /usr/bin/caddy
COPY Caddyfile /etc/caddy/Caddyfile

VOLUME /www

CMD ["caddy", "docker-proxy", "-caddyfile-path", "/etc/caddy/Caddyfile"]
